//#version 120
varying vec3 normal;
varying vec3 vertex_to_light_vector;
varying vec4 color;
varying vec4 vertexGuillermo;
uniform sampler2D myTexture;

uniform float ambientLevel;
varying vec2 texcoord;
varying float cubetype;

varying vec4 coord;


vec4 toonify(in float intensity) 
{ 
	vec4 color; 
	if (intensity > 0.98)
		color = vec4(0.8,0.8,0.8,1.0); 
	else if (intensity > 0.5) 
		color = vec4(0.4,0.4,0.8,1.0); 
	else if (intensity > 0.25) 
		color = vec4(0.2,0.2,0.4,1.0); 
	else color = vec4(0.1,0.1,0.1,1.0); 
	
	return(color); 
} 


void main()
{
	// Scaling The Input Vector To Length 1
	vec3 normalized_normal = normalize(normal);
	
	
	//if (color.b > 0.9)	
	//{
	//  normalized_normal = sin(normalized_normal) *200;
	//}else normalized_normal.y = normalized_normal.z * 20;
	
	//color  = vertex_to_light_vector * color.xyz;
	
	vec3 normalized_vertex_to_light_vector = normalize(vertex_to_light_vector);

	// Calculating The Diffuse Term And Clamping It To [0;1]
	float DiffuseTerm = clamp(dot(normal, vertex_to_light_vector), 0.0, 1.0);

	// Calculating The Final Color
	//color = toonify(1.0);
	
	
	if (cubetype >= 29.0)
	   gl_FragColor = color*texture2D(myTexture, texcoord)* (DiffuseTerm*(1-ambientLevel) + ambientLevel);
//	gl_FragColor = texture2D( Texture0 , vec2( gl_TexCoord[0] ) ) *color * (DiffuseTerm*(1-ambientLevel) + ambientLevel);
	else 	if (cubetype >= 19.0){
	gl_FragColor =  color * (DiffuseTerm*(1-ambientLevel) + ambientLevel);
	}
	
			gl_FragColor.a = color.a;

			
			
			
		
		//if (200.0 > 100.0)

};

