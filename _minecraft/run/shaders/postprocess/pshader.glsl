//version 120
uniform sampler2D Texture0;
uniform sampler2D Texture1;
uniform float screen_width;
uniform float screen_height;

varying float cubetype;

float LinearizeDepth(float z)
{
	float n = 0.5; // camera z near
  	float f = 1.0; // camera z far
  	return (2.0 * n) / (f + n - z * (f - n));
}

void main (void)
{
	float xstep = 1.0/screen_width;
	float ystep = 1.0/screen_height;
	float ratio = screen_width / screen_height;

	
	
	//vec4 color ;
	//	if (cubetype >= 25.0)
vec4 color1 = texture2D( Texture0 , vec2( gl_TexCoord[0].st ) );
//else
//vec4	color2 = texture2D( Texture0, vec2( gl_TexCoord[1].st ) );
	//float depth = texture2D( Texture0 , vec2( gl_TexCoord[0] ) ).r;	


	//Permet de scaler la profondeur
	//depth = LinearizeDepth(depth);

	gl_FragColor = color1;
}