//#version 120
varying vec3 normal;
varying vec3 vertex_to_light_vector;
varying vec4 color;
varying vec2 texcoord;

//uniform sampler2D Texture0;

//attribute vec4 coord;
varying vec4 coord;




uniform float elapsed;
uniform mat4 invertView;

varying vec3 randomnum;


varying vec4 vertexGuillermo;
varying float cubetype;


float rand()
{
  return 0.5 + 0.5 * 
     fract(sin(dot(vec2(gl_Vertex.x), vec2(12.9898,78.233)))* 43758.5453);
}


void main()
{
gl_TexCoord[0] = gl_MultiTexCoord0;
 gl_TexCoord[1] = gl_MultiTexCoord1;

//coord = gl_Texture_Coord;


vertexGuillermo =gl_Vertex;

vertexGuillermo = invertView * gl_ModelViewMatrix * vertexGuillermo;
	//oldvert = vertexGuillermo;

vec3 normalCopie = gl_Normal;

	cubetype = 0;




	if (gl_Color.b > 0.9)	
	{
	cubetype = 20.0;
	//coord.x = -1.0; 
		
		 //  texcoord = NULL;
		
	//gl_Position = gl_ModelViewProjectionMatrix * vertexGuillermo * 2;
	
	float randResult = rand();

	normalCopie.y = normalCopie.y - sin(-elapsed - vertexGuillermo.x/(2000) - vertexGuillermo.y/(10+randResult/2))*(8*randResult);

	vertexGuillermo.z = vertexGuillermo.z - sin(-elapsed - vertexGuillermo.x/(2000) - vertexGuillermo.y/(10+randResult/2))*(8*randResult) ;
	
		//vertexGuillermo.x = vertexGuillermo.x - sin(-elapsed - vertexGuillermo.x/(2000*(randResult/2)) - vertexGuillermo.y/(10+randResult/2))*(8*randResult) ;

	
	//normalCopie.y = normalCopie.y - sin(-elapsed - vertexGuillermo.x/(2000*rand()) - vertexGuillermo.y/(10*rand()))*6;
	//vertexGuillermo.z = vertexGuillermo.z - sin(-elapsed - vertexGuillermo.x/(2000*rand()) - vertexGuillermo.y/(10*rand()))*6*rand() ;	
	//	vertexGuillermo.z = vertexGuillermo.z + cos(elapsed + (vertexGuillermo.x*200) + vertexGuillermo.y*200)*5 ;
		
		
		
		
	
	

	}else{
	cubetype = 30.0;
	   texcoord = gl_MultiTexCoord0;

	normalCopie.y = normalCopie.y + sin(elapsed + (vertexGuillermo.x*vertexGuillermo.y)/2)*2;

	    normalCopie.x = normalCopie.y + sin(elapsed/2 + vertexGuillermo.x/40);
	
		//normalCopie.x = normalCopie.x + vertexGuillermo.x +vertexGuillermo.y;
	  // normalCopie.y = normalCopie.y + sin(elapsed + (vertexGuillermo.x*vertexGuillermo.y)/2)*10;
	  //  normalCopie.x = normalCopie.y + sin(elapsed/2 + vertexGuillermo.x/400);
	}
	

	//vertexGuillermo.z = vertexGuillermo.z + ;
	
	
	//pow(2,2)
	
	
	
	
	// Transforming The Vertex
	//gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_Position = gl_ProjectionMatrix * inverse(invertView) * vertexGuillermo;

	
	
	
	
	//gl_Position.xyz = gl_Position.zyx;
	
	// Transforming The Normal To ModelView-Space
	normal = gl_NormalMatrix * normalCopie; 

	//Direction lumiere
	vertex_to_light_vector = vec3(gl_LightSource[0].position);

	
		//vec4 colloolol = texture2D( Texture0 , vec2( gl_TexCoord[0] ) );


	//Couleur
	color = gl_Color;
	

	
}


