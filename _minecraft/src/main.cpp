//Includes application
#include <conio.h>
#include <vector>
#include <string>
#include <windows.h>

#include "external/gl/glew.h"
#include "external/gl/freeglut.h"

//Moteur
#include "engine/utils/types_3d.h"
#include "engine/timer.h"
#include "engine/log/log_console.h"
#include "engine/render/renderer.h"
#include "engine/gui/screen.h"
#include "engine/gui/screen_manager.h"




#include "cube.h"
#include "world.h"
#include "avatar.h"

NYRenderer * g_renderer = NULL;
NYTimer * g_timer = NULL;
int g_nb_frames = 0;
float g_elapsed_fps = 0;
int g_main_window_id;
int g_mouse_btn_gui_state = 0;
bool g_fullscreen = false;

NYAvatar * g_avatar;
NYWorld * g_world;


//Shaders
GLuint g_program;



//Soleil
NYVert3Df g_sun_dir;
NYColor g_sun_color;
float g_mn_lever = 6.0f * 60.0f;
float g_mn_coucher = 19.0f * 60.0f;
float g_tweak_time = 0;
bool g_fast_time = false;

//GUI 
GUIScreenManager * g_screen_manager = NULL;
GUIBouton * BtnParams = NULL;
GUIBouton * BtnClose = NULL;
GUILabel * LabelFps = NULL;
GUILabel * LabelCam = NULL;
GUIScreen * g_screen_params = NULL;
GUIScreen * g_screen_jeu = NULL;
GUISlider * g_slider;




GUILabel * LabelPosCam = NULL;
GUILabel * LabelDirCam = NULL;

//////////////////////////////////////////////////////////////////////////
// GESTION APPLICATION
//////////////////////////////////////////////////////////////////////////
void update(void)
{

	

	float elapsed = g_timer->getElapsedSeconds(true);


	//Tweak time
	if (g_fast_time)
		g_tweak_time += elapsed * 120.0f;

	static float g_eval_elapsed = 0;

	//Calcul des fps
	g_elapsed_fps += elapsed;
	g_nb_frames++;
	if(g_elapsed_fps > 1.0)
	{
		LabelFps->Text = std::string("FPS : ") + toString(g_nb_frames);
		g_elapsed_fps -= 1.0f;
		g_nb_frames = 0;
	
	}

	LabelPosCam->Text = std::string("Cam Position : ") + g_renderer->_Camera->_Position.toStr();
	LabelDirCam->Text = std::string("Cam Direction : ") + g_renderer->_Camera->_Direction.toStr();
	
	
	g_avatar->update(elapsed);
	
	
	//Rendu
	g_renderer->render(elapsed);
}


void render2d(void)
{
	g_screen_manager->render();
}

void renderObjects(void)
{
	SYSTEMTIME t;

	SetLocalTime(&t);

	//Rendu des axes
	//Active la lumi�re


	g_avatar->render();



	glPushMatrix();

	glDisable(GL_LIGHTING);





	glBegin(GL_LINES);
	
	glColor3d(1,0,0);
	glVertex3d(0,0,0);
	glVertex3d(10000,0,0);
	
	glColor3d(0,1,0);
	glVertex3d(0,0,0);
	glVertex3d(0,10000,0);

	glColor3d(0,0,1);
	glVertex3d(0,0,0);
	glVertex3d(0,0,10000);
	
	glEnd();
	glPopMatrix();

	glShadeModel(GL_SMOOTH);

	glEnable(GL_LIGHTING);
	//glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);

	glPushMatrix();
	//glRotatef(g_tweak_time*NYRenderer::_DeltaTimeCumul, 0, 0, 1);
	//glTranslatef(g_renderer->_Camera->_Position.X - 20, g_renderer->_Camera->_Position.Y - 60, g_renderer->_Camera->_Position.Z + 7);

	//Position du soleil
	glTranslatef(g_renderer->_Camera->_Position.X, g_renderer->_Camera->_Position.Y, g_renderer->_Camera->_Position.Z);
	glTranslatef(g_sun_dir.X * 1000, g_sun_dir.Y * 1000, g_sun_dir.Z * 1000);

	//Material du soleil : de l'emissive
	GLfloat sunEmissionMaterial[] = { 0.0, 0.0, 0.0, 1.0 };
	sunEmissionMaterial[0] = g_sun_color.R;
	sunEmissionMaterial[1] = g_sun_color.V;
	sunEmissionMaterial[2] = g_sun_color.B;
	glMaterialfv(GL_FRONT, GL_EMISSION, sunEmissionMaterial);


	//glColor3d(0.7, 0.6f, 0.3f);
	
	/*
	*/


	//GLfloat emissive[] = { 0.7, 0.0, 0.0, 1.0 };
	//glMaterialfv(GL_FRONT, GL_EMISSION, emissive);

	

	//SOLEIL
	glutSolidCube(50.0f);


	//On reset le material emissive pour la suite
	sunEmissionMaterial[0] = 0.0f;
	sunEmissionMaterial[1] = 0.0f;
	sunEmissionMaterial[2] = 0.0f;
	glMaterialfv(GL_FRONT, GL_EMISSION, sunEmissionMaterial);


	glPopMatrix();



	glPushMatrix();



	//glRotatef(NYRenderer::_DeltaTimeCumul * 50,0, 0, 1);
	glRotatef(NYRenderer::_DeltaTimeCumul*50.0f, 0, 1, 0);

	//Materiau sp�culaire, le meme pour tout le cube
	GLfloat whiteSpecularMaterial[] = { 0.3, 0.3, 0.3, 1.0 };
	glMaterialfv(GL_FRONT, GL_SPECULAR, whiteSpecularMaterial);
	GLfloat mShininess = 100;
	glMaterialf(GL_FRONT, GL_SHININESS, mShininess);




	glBegin(GL_QUADS);


	GLfloat materialDiffuse[] = { 0, 0.7, 0, 1.0 };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	GLfloat materialAmbient[] = { 0, 0.2, 0, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);





	glNormal3f(0,-1,0);
	//Face1
	glColor3d(0, 0.5f, 0);
	glVertex3f(-1, -1, -1);
	glVertex3f(1, -1, -1);
	glVertex3f(1, -1, 1);
	glVertex3f(-1, -1, 1);

	glNormal3f(0, 1, 0);
	//Face2
	glColor3d(0, 0.5f, 0);
	glVertex3f(-1, 1, -1);
	glVertex3f(-1, 1, 1);
	glVertex3f(1, 1, 1);
	glVertex3f(1, 1, -1);





	materialDiffuse[0] = 0.7f;
	materialDiffuse[1] = 0;
	materialDiffuse[2] = 0;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	materialAmbient[0] = 0.2f;
	materialAmbient[1] = 0;
	materialAmbient[2] = 0;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	glNormal3f(1, 0, 0);
	//Face3
	glColor3d(0.5f, 0, 0);
	glVertex3f(1, -1, -1);
	glVertex3f(1, 1, -1);
	glVertex3f(1, 1, 1);
	glVertex3f(1, -1, 1);

	glNormal3f(-1, 0, 0);
	//Face4
	glColor3d(0.5f, 0, 0);
	glVertex3f(-1, -1, -1);
	glVertex3f(-1, -1, 1);
	glVertex3f(-1, 1, 1);
	glVertex3f(-1, 1, -1);



	materialDiffuse[0] = 0;
	materialDiffuse[1] = 0;
	materialDiffuse[2] = 0.7f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	materialAmbient[0] = 0;
	materialAmbient[1] = 0;
	materialAmbient[2] = 0.2f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);



	//GLfloat emissive[] = { 0.3, 0.0, 0.0, 1.0 };
	//glMaterialfv(GL_FRONT, GL_EMISSION, emissive);
	//Speculaire
	//glMaterialfv(GL_FRONT, GL_SPECULAR, whiteSpecularMaterial);
	

	glNormal3f(0, 0, 1);
	//Face5
	glColor3d(0, 0, 0.5f);
	glVertex3f(-1, -1, 1);
	glVertex3f(1, -1, 1);
	glVertex3f(1, 1, 1);
	glVertex3f(-1, 1, 1);

	glNormal3f(0, 0, -1);
	//Face6
	glColor3d(0, 0, 0.5f);
	glVertex3f(-1, -1, -1);
	glVertex3f(-1, 1, -1);
	glVertex3f(1, 1, -1);
	glVertex3f(1, -1, -1);

	glEnd();


	glPopMatrix();

	glPushMatrix();



	GLfloat whiteSpecularMaterialSphere[] = { 0.3, 0.3, 0.3, 0.8 };
	glMaterialfv(GL_FRONT, GL_SPECULAR, whiteSpecularMaterialSphere);
	mShininess = 100;
	glMaterialf(GL_FRONT, GL_SHININESS, mShininess);

	GLfloat materialDiffuseSphere[] = { 0.7, 0.7, 0.7, 0.8 };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuseSphere);
	GLfloat materialAmbientSphere[] = { 0.2, 0.2, 0.2, 0.8 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbientSphere);

//	glutSolidSphere(2, 30, 30);



	//glDisable(GL_COLOR_MATERIAL);

//	float alpha[4] = { 0.7f, 0.7f, 0.7f, 0.3f };
	//glMaterialfv(GL_FRONT, GL_DIFFUSE, alpha);
	//glutSolidSphere(5, 30, 30);


	glPopMatrix();


	//Au lieu de rendre notre cube dans sa sph�re (mais on laisse le soleil)
	glPushMatrix();










	glUseProgram(g_program);

	GLuint elap = glGetUniformLocation(g_program, "elapsed");
	glUniform1f(elap, NYRenderer::_DeltaTimeCumul);

	GLuint amb = glGetUniformLocation(g_program, "ambientLevel");
	glUniform1f(amb, 0.5);

	GLuint invView = glGetUniformLocation(g_program, "invertView");
	glUniformMatrix4fv(invView, 1, true, g_renderer->_Camera->_InvertViewMatrix.Mat.t);

	//g_world->render_world_old_school();
	glPushMatrix();
	g_world->render_world_vbo();
	glPopMatrix();



	glPopMatrix();


}


bool getSunDirection(NYVert3Df & sun, float mnLever, float mnCoucher)
{
	bool nuit = false;

	SYSTEMTIME t;
	GetLocalTime(&t);

	//On borne le tweak time � une journ�e (cyclique)
	while (g_tweak_time > 24 * 60)
		g_tweak_time -= 24 * 60;

	//Temps �coul� depuis le d�but de la journ�e
	float fTime = (float)(t.wHour * 60 + t.wMinute + t.wSecond);
	fTime += g_tweak_time;
	while (fTime > 24 * 60)
		fTime -= 24 * 60;

	//Si c'est la nuit
	if (fTime < mnLever || fTime > mnCoucher)
	{
		nuit = true;
		if (fTime < mnLever)
			fTime += 24 * 60;
		fTime -= mnCoucher;
		fTime /= (mnLever + 24 * 60 - mnCoucher);
		fTime *= M_PI;
	}
	else
	{
		//c'est le jour
		nuit = false;
		fTime -= mnLever;
		fTime /= (mnCoucher - mnLever);
		fTime *= M_PI;
	}

	//Position en fonction de la progression dans la journ�e
	sun.X = cos(fTime);
	sun.Y = 0.2f;
	sun.Z = sin(fTime);
	sun.normalize();

	return nuit;
}


void setLightsBasedOnDayTime(void)
{
	//On active la light 0
	glEnable(GL_LIGHT0);

	//On recup la direciton du soleil
	bool nuit = getSunDirection(g_sun_dir, g_mn_lever, g_mn_coucher);

	//On d�finit une lumi�re directionelle (un soleil)
	float position[4] = { g_sun_dir.X, g_sun_dir.Y, g_sun_dir.Z, 0 }; ///w = 0 donc c'est une position a l'infini
	glLightfv(GL_LIGHT0, GL_POSITION, position);

	//Pendant la journ�e
	if (!nuit)
	{
		//On definit la couleur
		NYColor sunColor(1, 1, 0.8, 1);
		NYColor skyColor(0, 181.f / 255.f, 221.f / 255.f, 1);
		NYColor downColor(0.9, 0.5, 0.1, 1);
		sunColor = sunColor.interpolate(downColor, (abs(g_sun_dir.X)));
		skyColor = skyColor.interpolate(downColor, (abs(g_sun_dir.X)));

		g_renderer->setBackgroundColor(skyColor);

		float color[4] = { sunColor.R, sunColor.V, sunColor.B, 1 };
		glLightfv(GL_LIGHT0, GL_DIFFUSE, color);
		float color2[4] = { sunColor.R, sunColor.V, sunColor.B, 1 };
		glLightfv(GL_LIGHT0, GL_AMBIENT, color2);
		g_sun_color = sunColor;
	}
	else
	{
		//La nuit : lune blanche et ciel noir
		NYColor sunColor(1, 1, 1, 1);
		NYColor skyColor(0, 0, 0, 1);
		g_renderer->setBackgroundColor(skyColor);

		float color[4] = { sunColor.R / 3.f, sunColor.V / 3.f, sunColor.B / 3.f, 1 };
		glLightfv(GL_LIGHT0, GL_DIFFUSE, color);
		float color2[4] = { sunColor.R / 7.f, sunColor.V / 7.f, sunColor.B / 7.f, 1 };
		glLightfv(GL_LIGHT0, GL_AMBIENT, color2);
		g_sun_color = sunColor;
	}
}

void setLights(void)
{
	
	//On active la light 0
	//glEnable(GL_LIGHT0);
	/*
	//On d�finit une lumi�re directionelle (un soleil)
	float direction[4] = {0,0,1,0}; ///w = 0 donc elle est a l'infini
	glLightfv(GL_LIGHT0, GL_POSITION, direction );
	float color[4] = {0.5f,0.5f,0.5f};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, color );
	float color2[4] = {0.3f,0.3f,0.3f};
	glLightfv(GL_LIGHT0, GL_AMBIENT, color2 );
	float color3[4] = {0.3f,0.3f,0.3f};
	glLightfv(GL_LIGHT0, GL_SPECULAR, color3 );







	//Diffuse
	GLfloat materialDiffuse[] = { 0, 0.7, 0, 1.0 };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	*/






	/*

	//Emissive
	GLfloat emissive[] = { 1.0, 0.0, 0.0, 1.0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, emissive);

	//Ambient
	GLfloat materialAmbient[] = { 0, 0.2, 0, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);




	



	//On active l'illumination 
	glEnable(GL_LIGHTING);

	//On active la light 0
	glEnable(GL_LIGHT0);
	glEnable(GL_BLEND_SRC);
	*/
	//On d�finit une lumi�re 


	//NYRenderer::_DeltaTimeCumul * 100


	/*

	float position[4] = { g_renderer->_Camera->_Position.X - 20, g_renderer->_Camera->_Position.Y - 60, g_renderer->_Camera->_Position.Z + 7, 1 }; // w = 1 donc c'est une point light (w=0 -> directionelle, point � l'infini)
	glLightfv(GL_LIGHT0, GL_POSITION, position);

	float diffuse[4] = { 0.9f, 0.9f, 0.9f };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);

	float specular[4] = { 1.0f, 1.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

	float ambient[4] = { 0.1f, 0.1f, 0.1f };
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);

	*/









}

void resizeFunction(int width, int height)
{
	glViewport(0, 0, width, height);
	g_renderer->resize(width,height);
}

//////////////////////////////////////////////////////////////////////////
// GESTION CLAVIER SOURIS
//////////////////////////////////////////////////////////////////////////

void specialDownFunction(int key, int p1, int p2)
{
	//On change de mode de camera
	if(key == GLUT_KEY_LEFT)
	{
	}

}

void specialUpFunction(int key, int p1, int p2)
{

}

void keyboardDownFunction(unsigned char key, int p1, int p2)
{
	

	if(key == VK_ESCAPE)
	{
		glutDestroyWindow(g_main_window_id);	
		exit(0);
	}

	if(key == 'f')
	{
		if(!g_fullscreen){
			glutFullScreen();
			g_fullscreen = true;
		} else if(g_fullscreen){
			glutLeaveGameMode();
			glutLeaveFullScreen();
			glutReshapeWindow(g_renderer->_ScreenWidth, g_renderer->_ScreenWidth);
			glutPositionWindow(0,0);
			g_fullscreen = false;
		}
	}
	if (key == '+')
	{
		g_tweak_time+=10;
	}

	if (key == '-')
	{
		g_tweak_time-=10;
	}
		if(key =='g')	
		g_fast_time = !g_fast_time;
	
		if (key == 'w')
		{
			g_avatar->avance = true;
		}
		if (key == 'a')
		{
			g_avatar->gauche = true;

		}
		if (key == 's')
		{
			g_avatar->recule = true;

		}
		if (key == 'd')
		{
			g_avatar->droite = true;

		}
	

		if (key == VK_SPACE)
		{
			g_avatar->Jump = true;

		}


}

void keyboardUpFunction(unsigned char key, int p1, int p2)
{

	if (key == 'w')
	{
		g_avatar->avance = false;
	}
	if (key == 'a')
	{
		g_avatar->gauche = false;

	}
	if (key == 's')
	{
		g_avatar->recule = false;

	}
	if (key == 'd' )
	{
		g_avatar->droite = false;

	}
	
	
	if (key == VK_SPACE)
	{
		g_avatar->Jump = false;

	}


	
}

void mouseWheelFunction(int wheel, int dir, int x, int y)
{


	//POURQUOI? MARCHE PAS COMME EXEMPLE
	NYVert3Df wheeler = NYVert3Df(0, 0, dir);
	g_renderer->_Camera->move(wheeler*50);


	//g_renderer->_Camera->setLookAt(g_renderer->_Camera->_LookAt + wheeler);


	
}

void mouseFunction(int button, int state, int x, int y)
{
	//Gestion de la roulette de la souris
	if((button & 0x07) == 3 && state)
		mouseWheelFunction(button,1,x,y);
	if((button & 0x07) == 4 && state)
		mouseWheelFunction(button,-1,x,y);

	//GUI
	g_mouse_btn_gui_state = 0;
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		g_mouse_btn_gui_state |= GUI_MLBUTTON;
	
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{	



					int xCube;
					int yCube;
					int zCube;

					NYVert3Df  inter;
				
					if (g_world->getRayCollision(g_avatar->Position, g_avatar->Position + g_avatar->Cam->_Direction * 100, inter, xCube, yCube, zCube))
					{



						g_world->deleteCube(xCube, yCube, zCube);


					}
				
			
		
	}

	bool mouseTraite = false;
	mouseTraite = g_screen_manager->mouseCallback(x,y,g_mouse_btn_gui_state,0,0);
}

void mouseMoveFunction(int x, int y, bool pressed)
{

	bool mouseTraite = false;


	//NYVert3Df pos = g_renderer->_Camera->_Position;

	//g_renderer->_Camera->move(g_renderer->_Camera->_Position.);





	//mouseTraite = g_screen_manager->mouseCallback(x,y,g_mouse_btn_gui_state,0,0);
	
	
	//NYVert3Df vect = NYVert3Df(x, y, 0);

	
	//g_renderer->_Camera->setLookAt(g_renderer->_Camera->_LookAt+vect);

	//rotateAround(float angle)

	

	if(pressed && mouseTraite)
	{
		//Mise a jour des variables li�es aux sliders
	}

	



	////////////////////////////////////////////







	static int lastx = -1;
	static int lasty = -1;


		if (lastx == -1 && lasty == -1)
		{
			lastx = x;
			lasty = y;
		}

		int dx = x - lastx;
		int dy = y - lasty;

		lastx = x;
		lasty = y;

		if (GetKeyState(VK_LCONTROL) & 0x80)
		{
		
			NYVert3Df strafe = g_renderer->_Camera->_NormVec;
			strafe.Z = 0;
			strafe.normalize();
			strafe *= (float)-dx / 1.0f;

			NYVert3Df avance = g_renderer->_Camera->_Direction;
			avance.Z = 0;
			avance.normalize();
			avance *= (float)dy / 1.0f;


			g_renderer->_Camera->move(avance + strafe);
		}
		else
		{
			g_renderer->_Camera->rotate((float)-dx / 300.0f);
			g_renderer->_Camera->rotateUp((float)-dy / 300.0f);
		}

	

}

void mouseMoveActiveFunction(int x, int y)
{
	mouseMoveFunction(x,y,true);
}
void mouseMovePassiveFunction(int x, int y)
{
	mouseMoveFunction(x,y,false);
}


void clickBtnParams (GUIBouton * bouton)
{
	g_screen_manager->setActiveScreen(g_screen_params);
}

void clickBtnCloseParam (GUIBouton * bouton)
{
	g_screen_manager->setActiveScreen(g_screen_jeu);
}

/**
  * POINT D'ENTREE PRINCIPAL
  **/
int main(int argc, char* argv[])
{ 
	


	LogConsole::createInstance();

	int screen_width = 1920;
	int screen_height = 1080;

	glutInit(&argc, argv); 
	glutInitContextVersion(3,0);
	glutSetOption(
		GLUT_ACTION_ON_WINDOW_CLOSE,
		GLUT_ACTION_GLUTMAINLOOP_RETURNS
		);

	glutInitWindowSize(screen_width,screen_height);
	glutInitWindowPosition (0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE );

	glEnable(GL_MULTISAMPLE);

	Log::log(Log::ENGINE_INFO, (toString(argc) + " arguments en ligne de commande.").c_str());	
	bool gameMode = true;
	for(int i=0;i<argc;i++)
	{
		if(argv[i][0] == 'w')
		{
			Log::log(Log::ENGINE_INFO,"Arg w mode fenetre.\n");
			gameMode = false;
		}
	}

	if(gameMode)
	{
		int width = glutGet(GLUT_SCREEN_WIDTH);
		int height = glutGet(GLUT_SCREEN_HEIGHT);
		
		char gameModeStr[200];
		sprintf(gameModeStr,"%dx%d:32@60",width,height);
		glutGameModeString(gameModeStr);
		g_main_window_id = glutEnterGameMode();
	}
	else
	{
		g_main_window_id = glutCreateWindow("MyNecraft");
		glutReshapeWindow(screen_width,screen_height);
	}

	if(g_main_window_id < 1) 
	{
		Log::log(Log::ENGINE_ERROR,"Erreur creation de la fenetre.");
		exit(EXIT_FAILURE);
	}
	
	GLenum glewInitResult = glewInit();

	if (glewInitResult != GLEW_OK)
	{
		Log::log(Log::ENGINE_ERROR,("Erreur init glew " + std::string((char*)glewGetErrorString(glewInitResult))).c_str());
		_cprintf("ERROR : %s",glewGetErrorString(glewInitResult));
		exit(EXIT_FAILURE);
	}

	//Affichage des capacit�s du syst�me
	Log::log(Log::ENGINE_INFO,("OpenGL Version : " + std::string((char*)glGetString(GL_VERSION))).c_str());

	glutDisplayFunc(update);
	glutReshapeFunc(resizeFunction);
	glutKeyboardFunc(keyboardDownFunction);
	glutKeyboardUpFunc(keyboardUpFunction);
	glutSpecialFunc(specialDownFunction);
	glutSpecialUpFunc(specialUpFunction);
	glutMouseFunc(mouseFunction);
	glutMotionFunc(mouseMoveActiveFunction);
	glutPassiveMotionFunc(mouseMovePassiveFunction);
	glutIgnoreKeyRepeat(1);

	//NYRenderer::_DeltaTimeCumul * 100
	//Changement de la couleur de fond
	NYColor skyColor(0,  181.f / 255.f, 221.f / 255.f, 1);

	//Initialisation du renderer
	g_renderer = NYRenderer::getInstance();
	g_renderer->setRenderObjectFun(renderObjects);
	g_renderer->setRender2DFun(render2d);
	g_renderer->setLightsFun(setLightsBasedOnDayTime);
	g_renderer->setBackgroundColor(skyColor);
	g_renderer->initialise();

	//On applique la config du renderer
	glViewport(0, 0, g_renderer->_ScreenWidth, g_renderer->_ScreenHeight);
	g_renderer->resize(g_renderer->_ScreenWidth,g_renderer->_ScreenHeight);
	
	//Ecran de jeu
	uint16 x = 10;
	uint16 y = 10;
	g_screen_jeu = new GUIScreen(); 

	g_screen_manager = new GUIScreenManager();
		
	//Bouton pour afficher les params
	BtnParams = new GUIBouton();
	BtnParams->Titre = std::string("Params");
	BtnParams->X = x;
	BtnParams->setOnClick(clickBtnParams);
	g_screen_jeu->addElement(BtnParams);

	y += BtnParams->Height + 1;

	LabelFps = new GUILabel();
	LabelFps->Text = "FPS";
	LabelFps->X = x;
	LabelFps->Y = y;
	LabelFps->Visible = true;
	g_screen_jeu->addElement(LabelFps);


	LabelPosCam= new GUILabel();
	LabelPosCam->Text = "Cam Position";
	LabelPosCam->X = x;
	LabelPosCam->Y = y + 30;
	LabelPosCam->Visible = true;
	g_screen_jeu->addElement(LabelPosCam);


	LabelDirCam = new GUILabel();
	LabelDirCam->Text = "Cam Direction";
	LabelDirCam->X = x;
	LabelDirCam->Y = y + 60;
	LabelDirCam->Visible = true;
	g_screen_jeu->addElement(LabelDirCam);


	//Ecran de parametrage
	x = 10;
	y = 10;
	g_screen_params = new GUIScreen();

	GUIBouton * btnClose = new GUIBouton();
	btnClose->Titre = std::string("Close");
	btnClose->X = x;
	btnClose->setOnClick(clickBtnCloseParam);
	g_screen_params->addElement(btnClose);

	y += btnClose->Height + 1;
	y+=40;
	x+=10;

	GUILabel * label = new GUILabel();
	label->X = x;
	label->Y = y;
	label->Text = "Param :";
	g_screen_params->addElement(label);

	y += label->Height + 1;

	g_slider = new GUISlider();
	g_slider->setPos(x,y);
	g_slider->setMaxMin(1,0);
	g_slider->Visible = true;
	g_screen_params->addElement(g_slider);

	y += g_slider->Height + 1;
	y+=10;



	//Ecran a rendre
	g_screen_manager->setActiveScreen(g_screen_jeu);
	
	//Init Camera
	//g_renderer->_Camera->setPosition(NYVert3Df(100,100,100));
	//g_renderer->_Camera->setLookAt(NYVert3Df(0,0,0));
	

	//Fin init moteur

	//Init application


	//Init Timer
	g_timer = new NYTimer();
	
	//On start
	g_timer->start();


	//A la fin du main, on genere un monde
	g_world = new NYWorld();
	g_world->init_world();

	g_avatar = new NYAvatar(g_renderer->_Camera , g_world);



	//Dans le main, � l'initialisation du moteur de rendu

	//Active le post process (chargera le shader de post process "postprocess/pshader.glsl")
	g_renderer->initialise(true);

	//Creation d'un programme de shader, avec vertex et fragment shaders
	g_program = g_renderer->createProgram("shaders/psbase.glsl", "shaders/vsbase.glsl");



	glutMainLoop(); 


	return 0;
}


