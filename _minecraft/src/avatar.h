#ifndef __AVATAR__
#define __AVATAR__

#include "engine/utils/types_3d.h"
#include "engine/render/camera.h"
#include "world.h"

class NYAvatar
{
	public :
		NYVert3Df Position;
		NYVert3Df Speed;

		NYVert3Df MoveDir;
		bool Move;
		bool Jump;
		float Height;
		float Width;
		bool avance;
		bool recule;
		bool gauche;
		bool droite;
		bool Standing;

		NYCamera * Cam;
		NYWorld * World;

		NYAvatar(NYCamera * cam,NYWorld * world)
		{
			Position = NYVert3Df(30,70,150);
			Height = 5;
			Width = 5;
			Cam = cam;
			avance = false;
			recule = false;
			gauche = false;
			droite = false;
			Standing = false;
			Jump = false;
			World = world;

			Cam = cam;

			
			
		}


		void render(void)
		{
			//

			glPushMatrix();
			glTranslatef(Position.X, Position.Y, Position.Z);

			glutSolidCube(Width);


			glPopMatrix();

			glPushMatrix();
			glDisable(GL_LIGHTING);

			glBegin(GL_LINES);


			glColor3d(1, 0, 0);
			glVertex3d(Position.X, Position.Y, Position.Z);
			glVertex3d(Position.X + Cam->_Direction.X * 100, Position.Y + Cam->_Direction.Y * 100, Position.Z + Cam->_Direction.Z * 100);



			//glColor3d(0, 1, 0);
			//glVertex3d(Position.X, Position.Y, Position.Z);
			//glVertex3d(Cam->_Position.X + Cam->_Direction.X, Cam->_Position.Y + Cam->_Direction.Y, Cam->_Position.Z);

			glEnd();

			glPopMatrix();
		}


		void updateNEW(float elapsed)
		{
			//Par defaut, on applique la gravit� (-100 sur Z)
			NYVert3Df force = NYVert3Df(0, 0, -1) * 100.0f;

			//Si l'avatar n'est pas au sol, alors il ne peut pas sauter
			if (!Standing)
				Jump = false;


			//Si il est au sol, on applique les controles "ground"
			if (Standing)
			{
				if (avance)
					force += Cam->_Direction * 400;
				if (recule)
					force += Cam->_Direction * -400;
				if (gauche)
					force += Cam->_NormVec * -400;
				if (droite)
					force += Cam->_NormVec * 400;
			}
			else //Si il est en l'air, c'est du air control
			{
				if (avance)
					force += Cam->_Direction * 50;
				if (recule)
					force += Cam->_Direction * -50;
				if (gauche)
					force += Cam->_NormVec * -50;
				if (droite)
					force += Cam->_NormVec * 50;
			}

			//On applique le jump
			if (Jump)
			{
				force += NYVert3Df(0, 0, 1) * 50.0f / elapsed; //(impulsion, pas fonction du temps)
				Jump = false;
			}

			//On applique les forces en fonction du temps �coul�
			Speed += force * elapsed;

			//On met une limite a sa vitesse horizontale
			NYVert3Df horSpeed = Speed;
			horSpeed.Z = 0;
			if (horSpeed.getSize() > 70.0f)
			{
				horSpeed.normalize();
				horSpeed *= 70.0f;
				Speed.X = horSpeed.X;
				Speed.Y = horSpeed.Y;
			}

			//On le d�place, en sauvegardant son ancienne position
			NYVert3Df oldPosition = Position;
			Position += (Speed * elapsed);

			Standing = false;

			for (int i = 0; i < 3; i++)
			{
				float valueColMin = 0;
				NYAxis axis = World->getMinCol(Position, Width, Height, valueColMin, i);
				if (axis != 0)
				{
					valueColMin = max(abs(valueColMin), 0.001f) * (valueColMin > 0 ? 1.0f : -1.0f);
					if (axis & NY_AXIS_X)
					{
						Position.X += valueColMin;
						Speed.X = 0;
					}
					if (axis & NY_AXIS_Y)
					{
						Position.Y += valueColMin;
						Speed.Y = 0;
					}
					if (axis & NY_AXIS_Z)
					{
						Speed.Z = 0;
						Position.Z += valueColMin;
						Speed *= pow(0.01f, elapsed);
						Standing = true;
					}
				}
			}
		}









		//DIFFERENT SISTEME DE COLLISION
		void update(float elapsed)
		{
			Cam->setPosition(Position - (Cam->_Direction * 30));
			Cam->setLookAt(Position);
			//Cam->updateVecs();
		//	MoveDir = Cam->_Direction;



		//	Log::log(Log::ENGINE_INFO, "avance: " + avance);

			//Par defaut, on applique la gravit� (-100 sur Z)
			NYVert3Df force = NYVert3Df(0, 0, -1) * 100.0f;

			//Si l'avatar n'est pas au sol, alors il ne peut pas sauter
			if (!Standing)
				Jump = false;

			//Si il est au sol, on applique les controles "ground"
			if (Standing)
			{
				if (avance)
					force += Cam->_Direction * 400;
				if (recule)
					force += Cam->_Direction * -400;
				if (gauche)
					force += Cam->_NormVec * -400;
				if (droite)
					force += Cam->_NormVec * 400;
			}
			else //Si il est en l'air, c'est du air control
			{
				if (avance)
					force += Cam->_Direction * 50;
				if (recule)
					force += Cam->_Direction * -50;
				if (gauche)
					force += Cam->_NormVec * -50;
				if (droite)
					force += Cam->_NormVec * 50;
			}

			//On applique le jump
			if (Jump)
			{
				force += NYVert3Df(0, 0, 1) * 65.0f / elapsed; //(impulsion, pas fonction du temps)
				Jump = false;
			}

			//On applique les forces en fonction du temps �coul�
			Speed += force * elapsed;

			//Log::log(Log::ENGINE_INFO, ("standing: " + Standing));

			//Log::log(Log::ENGINE_INFO, ("Speed: " + Speed.toStr()).c_str());

			//On met une limite a sa vitesse horizontale
			NYVert3Df horSpeed = Speed;
			horSpeed.Z = 0;
			if (horSpeed.getSize() > 70.0f)
			{
				horSpeed.normalize();
				horSpeed *= 70.0f;
				Speed.X = horSpeed.X;
				Speed.Y = horSpeed.Y;
			}

			//On le d�place, en sauvegardant son ancienne position
			NYVert3Df oldPosition = Position;
			Position += (Speed * elapsed);

			//On recup la collision a la nouvelle position
			NYCollision collidePrinc = 0x00;
			NYCollision collide = World->collide_with_world(Position, Width, Height, collidePrinc);
			if (collide & NY_COLLIDE_BOTTOM && Speed.Z < 0)
			{
				Position.Z = oldPosition.Z;
				Speed *= pow(0.01f, elapsed);
				Speed.Z = 0;
				Standing = true;
			}
			else
				Standing = false;

			if (collide & NY_COLLIDE_UP && !Standing && Speed.Z > 0)
			{
				Position.Z = oldPosition.Z;
				Speed.Z = 0;
			}

			//On a regle le probleme du bottom et up, on g�re les collision sur le plan (x,y)
			collide = World->collide_with_world(Position, Width, Height, collidePrinc);

			//En fonction des cot�s, on annule une partie des d�placements
			if (collide & NY_COLLIDE_BACK && collide & NY_COLLIDE_RIGHT && collide & NY_COLLIDE_LEFT)
			{
				Position.Y = oldPosition.Y;
				Speed.Y = 0;
			}

			if (collide & NY_COLLIDE_FRONT && collide & NY_COLLIDE_RIGHT && collide & NY_COLLIDE_LEFT)
			{
				Position.Y = oldPosition.Y;
				Speed.Y = 0;
			}

			if (collide & NY_COLLIDE_RIGHT && collide & NY_COLLIDE_FRONT && collide & NY_COLLIDE_BACK)
			{
				Position.X = oldPosition.X;
				Speed.X = 0;
			}

			if (collide & NY_COLLIDE_LEFT && collide & NY_COLLIDE_FRONT && collide & NY_COLLIDE_BACK)
			{
				Position.X = oldPosition.X;
				Speed.X = 0;
			}

			//Si je collide sur un angle
			if (!(collide & NY_COLLIDE_BACK && collide & NY_COLLIDE_FRONT) && !(collide & NY_COLLIDE_LEFT && collide & NY_COLLIDE_RIGHT))
				if (collide & (NY_COLLIDE_BACK | NY_COLLIDE_FRONT | NY_COLLIDE_RIGHT | NY_COLLIDE_LEFT))
				{
					Position.Y = oldPosition.Y;
					Position.X = oldPosition.X;
				}

			/*
			float SpeedX = 0;
			float SpeedY = 0;

			if (avance)
			{
				SpeedX = 10;
			}

			if (recule)
			{
				SpeedX = -10;
			}

			if (gauche)
			{	
				SpeedY = -10;
			}

			if (droite)
			{
				SpeedY = 10;
			}

			Position.X += MoveDir.X * SpeedX;
			Position.Y += MoveDir.Y * SpeedX;
			//Position.Z += MoveDir.Z * SpeedX;

			Position.X += Cam->_NormVec.X * SpeedY;
			Position.Y += Cam->_NormVec.Y * SpeedY;
			//Position.Z += Cam->_NormVec.Z * SpeedY;

			Cam->setPosition(Position - (Cam->_Direction * 100));
			Cam->setLookAt(Position);
			Cam->updateVecs();
			MoveDir = Cam->_Direction;
			*/
		}



		











};

#endif